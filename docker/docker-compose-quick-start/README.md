# Docker compose quick start

_Данный quick start был протестирован только на Raspberry Pi (Raspbian, arch linux). Для работоспособности на других 
системах необходимо использовать соответсвующие образы homebridge и mosquitto. Образы noolite-mtrf-mqtt и 
noolite-mqtt-webserver являются кроссплатформенными._

Сервисы:
    
* mosquitto - mqtt брокер
* homebridge - homebrige с дополнительными характеристиками noolite
* [noolite-mtrf-mqtt](https://bitbucket.org/AlekseevAV/noolite-mtrf-to-mqtt) - ретранслятор сообщений с последовательного порта MTRF в MQTT сообщения
* [noolite-mqtt-webserver](https://bitbucket.org/AlekseevAV/noolite-mqtt-web-server) - веб интерфейс для конфигурирования NooLite устройств
 

### Запуск

1. Скопировать всю дерикторию docker-compose-quick-start на целевую систему

2. Установить Docker и Docker-compose (https://github.com/oznu/docker-homebridge/wiki/Homebridge-on-Raspberry-Pi)

3. Убедиться, что NooLite MTRF адаптер вставлен в usb порт и найти его наименование в системе. Обычно он монтируется c
наименованием `/dev/ttyXXXXX`. Данное наименование нужно указать в 30 строке docker-compose.yml вместо `/dev/ttyUSB0`

``` sh
"/dev/ttyUSB0:/dev/tty.mtrf_serial_port" -> "/dev/ttyXXXXX:/dev/tty.mtrf_serial_port"
```

3. Выполнить:

``` sh
docker-compose up
```

### Настройки

Конфигурационный файл homebridge находится в `homebridge/config.json`
Дополнительные плагины, которые будут установлены в homebrige, перед запуском указываются в файле `homebridge/startup.sh`
