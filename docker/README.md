docker-compose-quick-start - полный набор сервисов для быстрого старта

[homebridge_noolite](https://hub.docker.com/r/alekseevav/homebridge-noolite/) - оригинальный образ homebridge с 
дополнительными характеристиками для работы с NooLite

[noolite_mqtt_web_server](https://hub.docker.com/r/alekseevav/noolite-mqtt-webserver/) - образ noolite веб сервера