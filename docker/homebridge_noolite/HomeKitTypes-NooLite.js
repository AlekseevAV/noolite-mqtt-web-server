/**
  * Дополнительный сервис и характеристики для NooLite
  * Куда и как это добавить расписано тут: https://github.com/cflurin/homebridge-mqtt/issues/2
  */

var inherits = require('util').inherits;
var Characteristic = require('../Characteristic').Characteristic;
var Service = require('../Service').Service;


/**
  * Characteristic "NooLite Channel"
  */

Characteristic.NooLiteChannel = function() {
  Characteristic.call(this, 'NooLiteChannel', '00000102-0000-1000-8000-0026BB765291');
  this.setProps({
    format: Characteristic.Formats.INT,
    perms: [Characteristic.Perms.READ, Characteristic.Perms.NOTIFY]
  });
  this.value = this.getDefaultValue();
};

inherits(Characteristic.NooLiteChannel, Characteristic);

Characteristic.NooLiteChannel.UUID = '00000102-0000-1000-8000-0026BB765291';

/**
  * Characteristic "NooLite ID"
  */

Characteristic.NooLiteId = function() {
  Characteristic.call(this, 'NooLiteId', '00000102-0000-1000-8000-0026BB765292');
  this.setProps({
    format: Characteristic.Formats.STRING,
    perms: [Characteristic.Perms.READ, Characteristic.Perms.NOTIFY]
  });
  this.value = this.getDefaultValue();
};

inherits(Characteristic.NooLiteId, Characteristic);

Characteristic.NooLiteId.UUID = '00000102-0000-1000-8000-0026BB765292';


/**
 * Service "NooLite service"
 */

Service.NooLiteDevice = function(displayName, subtype) {
  Service.call(this, displayName, '00000101-0000-1000-8000-0026BB765291', subtype);

  // Required Characteristics
  this.addCharacteristic(Characteristic.NooLiteChannel);

  // Optional Characteristics
  this.addOptionalCharacteristic(Characteristic.NooLiteId);
};

inherits(Service.NooLiteDevice, Service);

Service.NooLiteDevice.UUID = '00000101-0000-1000-8000-0026BB765291';
